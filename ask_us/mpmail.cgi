#!/usr/bin/perl

##############################################################################
# Form Mail CGI
# （CGI本体）
# Copyright(C)  2001 - 2003
# http://www.u-bin.com/
###############################################################################
use strict;
require './jcode.pl';
require './mpconfig.cgi';
use CGI;
my $q = new CGI;

# 設定ファイル読み込み
my $MAILTO = &mpconfig::MAILTO;
my $FROM = &mpconfig::FROM;
my $REDIRECT_URL = &mpconfig::REDIRECT_URL;
my $SENDMAIL = &mpconfig::SENDMAIL;
my @NECESSARY_NAMES = &mpconfig::NECESSARY_NAMES;
my $SUBJECT = &mpconfig::SUBJECT;
my $MANUAL_CGIURL = &mpconfig::MANUAL_CGIURL;
my $CONFIRM_FLAG = &mpconfig::CONFIRM_FLAG;
my $FORMAT_CUSTOM_FLAG = &mpconfig::FORMAT_CUSTOM_FLAG;
my $MAIL_TEMP_FILE = &mpconfig::MAIL_TEMP_FILE;
my $REPLY_FLAG = &mpconfig::REPLY_FLAG;
my $FROM_ADDR_FOR_REPLY = &mpconfig::FROM_ADDR_FOR_REPLY;
my $SENDER_NAME_FOR_REPLY = &mpconfig::SENDER_NAME_FOR_REPLY;
my $SUBJECT_FOR_REPLY = &mpconfig::SUBJECT_FOR_REPLY;
my $CONFIRM_TEMP_FILE = &mpconfig::CONFIRM_TEMP_FILE;
my $ERROR_TEMP_FILE = &mpconfig::ERROR_TEMP_FILE;
my $ATTACHMENT_DIR = &mpconfig::ATTACHMENT_DIR;
my $REPLY_TEMP_FILE = &mpconfig::REPLY_TEMP_FILE;
my $ATTACHMENT_DEL_FLAG = &mpconfig::ATTACHMENT_DEL_FLAG;
my $ATTACHMENT_MAX_SIZE = &mpconfig::ATTACHMENT_MAX_SIZE;
my $MAX_INPUT_CHAR = &mpconfig::MAX_INPUT_CHAR;
my $LOGING_FLAG = &mpconfig::LOGING_FLAG;
my $LOGFILE = &mpconfig::LOGFILE;
my $LOG_FORMAT = &mpconfig::LOG_FORMAT;
my $DELIMITER = &mpconfig::DELIMITER;
my $SIRIAL_FLAG = &mpconfig::SIRIAL_FLAG;
my @REJECT_HOSTS = &mpconfig::REJECT_HOSTS;
my $REJECT_ERR_MSG = &mpconfig::REJECT_ERR_MSG;
my @EXT_RESTRICT = &mpconfig::EXT_RESTRICT;
my @ALLOW_FROM_URLS = &mpconfig::ALLOW_FROM_URLS;
my $WRAP = &mpconfig::WRAP;
my %NAME_MAP = &mpconfig::NAME_MAP;
my $ERRORS_TO = &mpconfig::ERRORS_TO;


# mpconfig.cgiの設定内容のチェック
&ConfCheck;

# 外部サーバからの利用禁止
&ExternalRequestCheck;

# X-Mailerの指定
my $X_Mailer = 'Form Mail CGI(http://www.u-bin.com/)';

# Base64エンコードテーブル
my $Base64Table = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
	           'abcdefghijklmnopqrstuvwxyz'.
	           '0123456789+/';

# 指定ホストからのアクセスを除外する
my $HostName = &GetHostName($ENV{'REMOTE_ADDR'});
&RejectHostAccess($HostName);

# フォームデータを取得,送信メール本文生成、確認画面用HIDDENタグ生成
my @DataNames = $q->param;
my $Message = '';
my %EucValues = ();
my @EucNames = ();
my @HiddenTags = ();
# 添付ファイルが存在すれば 1 がセットされる
my $AttachmentFlag = 0;
if($q->param('attachment')) {
	$AttachmentFlag = 1;
}

my $Status = $q->param('status');


# タイムスタンプ用の日時文字列を取得
# $Stamp: ログ出力用
# $SendDate: 送信メール用
my($Stamp, $SendDate) = &GetDate;

# シリアル番号を生成
my $SirialNo;
if($SIRIAL_FLAG) {$SirialNo = &MakeSirial;}

if($SIRIAL_FLAG) {
	$Message = "【シリアル番号】\n";
	$Message .= "$SirialNo\n\n";
}

my $key;
for $key (@DataNames) {
	if($key eq 'status') {next;}
	my @DataTmpList = $q->param($key);
	my $ItemNum = scalar @DataTmpList;
	&jcode::convert(\$key, "sjis");
	if($NAME_MAP{$key}) {
		$Message .= "【$NAME_MAP{$key}】\n";
	} else {
		$Message .= "【$key】\n";
	}
	my $value = '';
	my $SelectedItem;
	for $SelectedItem (@DataTmpList) {
		if($ItemNum > 1) {
			unless($SelectedItem) {
				next;
			}
		}
		&jcode::convert(\$SelectedItem, "sjis");
		unless($key eq 'attachment') {
			if($MAX_INPUT_CHAR && length($SelectedItem) > $MAX_INPUT_CHAR) {
				&ErrorPrint("入力文字は、半角で$MAX_INPUT_CHAR文字までです。");
			}
		}
		$Message .= "$SelectedItem\n";
		$value .= "$SelectedItem ";
		$SelectedItem =~ s/\</&lt;/g;
		$SelectedItem =~ s/\>/&gt;/g;
		$SelectedItem =~ s/\"/&quot;/g;
		my $HiddenTag = "<input type=\"hidden\" name=\"$key\" value=\"$SelectedItem\">";
		push(@HiddenTags, $HiddenTag);
	}
	$value =~ s/ $//;
	&jcode::convert(\$value, "euc", "sjis");
	my $euckey = $key;
	&jcode::convert(\$euckey, "euc", "sjis");
	push(@EucNames, $euckey);
	$EucValues{$euckey} = $value;

	# メッセージの改行コードを統一する
	$Message = &UnifyReturnCode($Message);
	$Message .= "\n";
}
push(@HiddenTags, '<input type="hidden" name="status" value="1">');

# 必須項目が選択もしくは入力されているかのチェック
&NecessaryCheck(\%EucValues, \@NECESSARY_NAMES);

# name属性が「mailaddress」のものが存在しなければ、自動返信のフラグをOFFにする。
unless($EucValues{'mailaddress'}) {
	$REPLY_FLAG = 0;
}

# フォーム内に、name属性が「mailaddress」のものがあれば、
# 送信元メールアドレスを優先的に設定する。
my $FromFlag = 0;
my $FromTmp = $EucValues{'mailaddress'};
if($FromTmp) {
	$FROM = $FromTmp;
	$FromFlag = 1;
}

# フォーム内に、name属性が「subject」のものがあれば、
# 送信元メールサブジェクトを優先的に設定する。
my $SubjectTmp = $q->param('subject');
if($SubjectTmp) {$SUBJECT = $SubjectTmp;}

# サブジェクト内に、フォーム入力値変換指示子があれば変換する。
&jcode::convert(\$SUBJECT, 'euc');
for $key (keys %EucValues) {
	$SUBJECT =~ s/\$$key\$/$EucValues{$key}/g;
	$SUBJECT =~ s/<!--$key-->/$EucValues{$key}/g;
}

# 添付ファイルがあれば、ファイル名を抽出し、
# テンポラリーファイルを生成する。
my($AttachContentType, $AttachmentFileName, $AttachmentFile);
if($AttachmentFlag) {
	my $FullFileName = $q->param('attachment');
	$AttachContentType = $q->uploadInfo($FullFileName)->{'Content-Type'};
	my @TempList = split(/\\/, $FullFileName);
	$AttachmentFileName = pop(@TempList);

	# 拡張子チェック
	if(scalar @EXT_RESTRICT) {
		my @FileNameParts = split(/\./, $AttachmentFileName);
		my $ext = pop(@FileNameParts);
		unless(grep(/^$ext$/i, @EXT_RESTRICT)) {
			&ErrorPrint("指定のファイルを送信することはできません。");
		}
	}

	$ATTACHMENT_DIR =~ s/\/$//;
	$ATTACHMENT_DIR .= '/';
	$AttachmentFile = $ATTACHMENT_DIR.$AttachmentFileName;
	open(OUTFILE, ">$AttachmentFile") || &ErrorPrint("添付ファイルのテンポラリーファイル\[$AttachmentFile\]の作成に失敗しました。:$!");
	my $bytesread;
	my $buffer;
	while($bytesread=read($FullFileName, $buffer, 1024)) {
		print OUTFILE $buffer;
	}
	close(OUTFILE);

	# 添付ファイルサイズのチェック
	my $AttachmentFileSize = -s $AttachmentFile;
	if($AttachmentFileSize > $ATTACHMENT_MAX_SIZE) {
		unlink($AttachmentFile);
		&ErrorPrint("$ATTACHMENT_MAX_SIZEバイト以上のファイルは添付できません。");
	}
}

if($CONFIRM_FLAG && $AttachmentFlag == 0 && $Status eq '') {
	&PrintConfirm;
	exit;
} else {
	&MailSend;
}

# 返信メールを送信
if($REPLY_FLAG && $FromFlag) {
	&Reply($REPLY_TEMP_FILE);
}

# フォームデータをログに出力
if($LOGING_FLAG) {
	&Loging(\@EucNames, \%EucValues);
}

# 添付ファイルを削除する
if($AttachmentFlag && $ATTACHMENT_DEL_FLAG) {
	unlink($AttachmentFile);
}

# リダイレクト
select(STDOUT);
print "Location: $REDIRECT_URL\n\n";

exit;


######################################################################
# サブルーチン
######################################################################

# 必須項目が選択もしくは入力されているかのチェック
sub NecessaryCheck {
	my($EucValues, $NecessaryNames) = @_;
	my($Name, $ErrorString, $NoData);
	my(@EucNecessaryNames);
	for $Name (@$NecessaryNames) {
		&jcode::convert(\$Name, "euc");
		push(@EucNecessaryNames, $Name);
	}

	my @NoDataList = ();
	my $NoDataCnt = 0;
	for $Name (@EucNecessaryNames) {
		if($$EucValues{$Name} eq '') {
			push(@NoDataList, $Name);
			$NoDataCnt ++;
		}
		if($Name eq 'mailaddress') {
			&MailAddressCheck($$EucValues{$Name});
		}
	}
	if($NoDataCnt) {
		for $NoData (@NoDataList) {
			&jcode::convert(\$NoData, "sjis");
			if($NAME_MAP{$NoData}) {
				$ErrorString .= '「'.$NAME_MAP{$NoData}.'」';
			} else {
				$ErrorString .= '「'.$NoData.'」';
			}
		}
		$ErrorString .= 'は必須項目です。';
		&ErrorPrint($ErrorString);
	}
}


# 確認画面を表示する
sub PrintConfirm {

	# このCGIのURLを特定する。
	my $CgiUrl;
	if($MANUAL_CGIURL =~ /^http/) {
		$CgiUrl = $MANUAL_CGIURL;
	} else {
		$CgiUrl = &GetCgiUrl;
	}

	#Hiddenタグを一つのスカラー変数に格納する。
	my($Hidden) = join("\n", @HiddenTags);
		
	my $filesize = -s $CONFIRM_TEMP_FILE;
	if(!open(TEMP, "$CONFIRM_TEMP_FILE")) {
		&ErrorPrint("確認画面用テンプレートHTMLファイル（$CONFIRM_TEMP_FILE）をオープンできませんでした。");
		exit;
	}
	binmode(TEMP);
	my $HtmlData;
	sysread(TEMP, $HtmlData, $filesize);
	close(TEMP);
	my $HiddenFlag = 0;
	my $CgiUrlFlag = 0;
	if($HtmlData =~ /<!--hidden-->/ || $HtmlData =~ /\$hidden\$/) {
		$HtmlData =~ s/\$hidden\$/$Hidden/;
		$HtmlData =~ s/<!--hidden-->/$Hidden/;
		$HiddenFlag = 1;
	}
	unless($HiddenFlag) {
		&ErrorPrint("確認画面用テンプレートHTMLファイル（$CONFIRM_TEMP_FILE）に、\$hidden\$ または &lt;!--hidden--&gt; が記載されておりません。");
	}
	$HtmlData =~ s/\$cgiurl\$/$CgiUrl/;
	$HtmlData =~ s/<!--cgiurl-->/$CgiUrl/;
	&jcode::convert(\$HtmlData, "euc");
	my $EucName;
	for $EucName (keys(%EucValues)) {
		my $EucValue;
		$EucValue = $EucValues{$EucName};
		$EucValue =~ s/\</&lt;/g;
		$EucValue =~ s/\>/&gt;/g;
		$EucValue =~ s/\"/&quot;/g;
		$EucValue =~ s/\n/<br>/g;
		unless($EucValue) {
			$EucValue = '&nbsp;';
		}
		$HtmlData =~ s/\$$EucName\$/$EucValue/g;
		$HtmlData =~ s/<!--$EucName-->/$EucValue/g;
	}

	&jcode::convert(\$HtmlData, "sjis");
	print $q->header(-type=>'text/html; charset=Shift_JIS');
	print "$HtmlData\n";
	exit;

}

# メールを送信する
sub MailSend {
	# 送信者情報をメール本文に追加
	$Message .= "【送信者情報】\n";
	$Message .= "  ・ブラウザー      : $ENV{'HTTP_USER_AGENT'}\n";
	$Message .= "  ・送信元IPアドレス: $ENV{'REMOTE_ADDR'}\n";
	$Message .= "  ・送信元ホスト名  : $HostName\n";
	$Message .= "  ・送信日時        : $SendDate\n";

	if($FORMAT_CUSTOM_FLAG) {
		$Message = &CustomizeMail(\@EucNames, \%EucValues);
	}

	# メッセージをワードラップする
	if($WRAP >= 50) {
		$Message = &WordWrap($Message);
	}

	# メッセージをJISに変換
	&jcode::convert(\$Message, "jis");
	
	# メールタイトルをBase64 Bエンコード
	$SUBJECT = &EncodeSubject($SUBJECT);
	
	my($Boundary, $Base64EncodedData);
	if($AttachmentFlag) {
		# 境界を定義
		$Boundary = &GenerateBoundary($Message);
		#添付ファイルをBase64エンコードする
		$Base64EncodedData = &Base64Encode($AttachmentFile);
	}
	
	# メール送信
	open(SENDMAIL, "|$SENDMAIL -t");
	select(SENDMAIL);
	print "To: $MAILTO\n";
	print "From: $FROM\n";
	if($ERRORS_TO) {
		print "Errors-To: $ERRORS_TO\n";
	}
	print "Subject: $SUBJECT\n";
	print "X-Mailer: $X_Mailer\n";
	print "MIME-Version: 1.0\n";

	if($AttachmentFlag) {
		print 'Content-Type: multipart/mixed;',"\n";
		print "	boundary=\"$Boundary\"\n";
		print "Content-Transfer-Encoding: 7bit\n";
		print "\n";
		print "--$Boundary\n";
	}
	print "Content-Type: text/plain\; charset=iso-2022-jp\n";
	print "Content-Transfer-Encoding: 7bit\n";
	print "\n";
	print "$Message\n";
	print "\n";

	if($AttachmentFlag) {
		print "--$Boundary\n";
		print "Content-Type: $AttachContentType; name=\"$AttachmentFileName\"\n";
		print "Content-Disposition: attachment;\n";
		print " filename=\"$AttachmentFileName\"\n";
		print "Content-Transfer-Encoding: base64\n";
		print "\n";
		print "$Base64EncodedData\n";
		print "\n";
		print "--$Boundary--";
	}
	close(SENDMAIL);
}


sub CustomizeMail {
	my($EucNames, $EucValues) = @_;
	my $size = -s $MAIL_TEMP_FILE;
	if(!open(IN, "$MAIL_TEMP_FILE")) {
		&ErrorPrint("テンプレートファイルをオープンできませんでした。: $!");
		exit;
	}
	my $buf;
	sysread(IN, $buf, $size);
	close(IN);
	&jcode::convert(\$buf, 'euc');
	my $key;
	for $key (@$EucNames) {
		$buf =~ s/\$$key\$/$EucValues->{$key}/g;
		$buf =~ s/<!--$key-->/$EucValues->{$key}/g;
	}
	$buf =~ s/\$USERAGENT\$/$ENV{'HTTP_USER_AGENT'}/g;
	$buf =~ s/\$REMOTE_ADDR\$/$ENV{'REMOTE_ADDR'}/g;
	$buf =~ s/\$REMOTE_HOST\$/$HostName/g;
	$buf =~ s/\$DATE\$/$SendDate/g;
	$buf =~ s/\$SIRIAL\$/$SirialNo/ig;

	$buf =~ s/<!--USERAGENT-->/$ENV{'HTTP_USER_AGENT'}/g;
	$buf =~ s/<!--REMOTE_ADDR-->/$ENV{'REMOTE_ADDR'}/g;
	$buf =~ s/<!--REMOTE_HOST-->/$HostName/g;
	$buf =~ s/<!--DATE-->/$SendDate/g;
	$buf =~ s/<!--SIRIAL-->/$SirialNo/ig;
	$buf = &UnifyReturnCode($buf);
	&jcode::convert(\$buf, 'sjis', 'euc');
	return $buf;
}


# フォームデータをログに出力
sub Loging {
	my($EucNames, $EucValues) = @_;
	my($key, $LogString, $DelimiterChar);
	if($DELIMITER == 2) {
		$DelimiterChar = ' ';
	} elsif($DELIMITER == 3) {
		$DelimiterChar = "\t";
	} else {
		$DelimiterChar = ",";
	}

	my($ValueBuff);
	$LogString .= "$Stamp$DelimiterChar";
	if($SIRIAL_FLAG) {
		$LogString .= "$SirialNo$DelimiterChar";
	}
	for $key (@$EucNames) {
		if($LOG_FORMAT) {$LogString .= "$key=";}
		$ValueBuff = $$EucValues{$key};
		if($DELIMITER == 2) {
			$ValueBuff = &EucZenSpace($ValueBuff);	#半角スペースをEUC全角スペースに変換
		} elsif($DELIMITER == 3) {
			$ValueBuff =~ s/\t//g;
		} else {
			$ValueBuff = &EucZenKanma($ValueBuff);	#半角カンマをEUC全角カンマに変換
		}
		$LogString .= "$ValueBuff";
		$LogString .= $DelimiterChar;
	}
	$LogString =~ s/$DelimiterChar$//;
	$LogString = &UnifyReturnCode($LogString);	#改行コードを「\n」に統一する
	$LogString =~ s/\n/<br>/g;	#改行コードを<br>に変換する
	&jcode::convert(\$LogString, "sjis");	#SJISに変換
	open(LOGFILE, ">>$LOGFILE") || &ErrorPrint("ログファイル $LOGFILE を書込オープンできませんでした。： $!");
	unless(&Lock(*LOGFILE)) {
		&ErrorPrint("只今、込み合っております。しばらくしてからお試しください。");;
	}
	print LOGFILE "$LogString\n";
	close(LOGFILE);
}


# 返信メールを送信する
sub Reply {
	my($ReplyTempFile) = @_;
	my($key, $Value, $Message);
	# テンプレートファイルの文字列を $Messageに格納する
	if(!open(IN, "$ReplyTempFile")) {
		&ErrorPrint("テンプレートファイルをオープンできませんでした。: $!");
		exit;
	}
	my $size = -s $ReplyTempFile;
	sysread(IN, $Message, $size);
	close(IN);
	# メッセージの改行コードを統一する
	$Message = &UnifyReturnCode($Message);
	# 文字列変換のために、メッセージをEUCに変換
	&jcode::convert(\$Message, "euc");
	# 文字変換
	my($value);
	for $key (keys(%EucValues)) {
		$value = $EucValues{$key};
		$value = &UnifyReturnCode($value);
		$Message =~ s/\$$key\$/$value/g;
		$Message =~ s/<!--$key-->/$value/g;
		if($SIRIAL_FLAG) {
			$Message =~ s/\$SIRIAL\$/$SirialNo/ig;
			$Message =~ s/<!--SIRIAL-->/$SirialNo/ig;
		}
	}
	# メッセージをワードラップする
	if($WRAP >= 50) {
		$Message = &WordWrap($Message);
	}
	# メール送信のために、メッセージをJISに変換
	&jcode::convert(\$Message, "jis");
	# メールタイトルをBase64 Bエンコード
	$SUBJECT_FOR_REPLY = &EncodeSubject($SUBJECT_FOR_REPLY);
	# From行をエンコード
	my $FromLine = $FROM_ADDR_FOR_REPLY;
	if($SENDER_NAME_FOR_REPLY) {
		$FromLine = &EncodeFrom($SENDER_NAME_FOR_REPLY, $FROM_ADDR_FOR_REPLY);
	}
	# 返信メール送信
	open(SENDMAIL, "|$SENDMAIL -t");
	select(SENDMAIL);
	print "To: $FROM\n";
	print "From: $FromLine\n";
	if($ERRORS_TO) {
		print "Errors-To: $ERRORS_TO\n";
	}
	print "Subject: $SUBJECT_FOR_REPLY\n";
	print "X-Mailer: $X_Mailer\n";
	print "MIME-Version: 1.0\n";
	print "Content-Type: text/plain\; charset=iso-2022-jp\n";
	print "Content-Transfer-Encoding: 7bit\n";
	print "\n";
	print "$Message\n";
	close(SENDMAIL);
}


# From行をエンコード
sub EncodeFrom {
	my($Str, $FromAddr) = @_;
	my $EncStr = &EncodeSubject($Str);
	my @TmpArray = split(/\n/, $EncStr);
	my $Tmp = pop(@TmpArray);
	if(length($Tmp) + length($FromAddr) + 3 > 75) {
		$EncStr .= "\n";
	}
	$EncStr .= " <$FromAddr>";
	return $EncStr;
}


sub ErrorPrint {
	my($msg) = @_;
	unless(-e $ERROR_TEMP_FILE) {
		&ErrorPrint2("テンプレートファイル $ERROR_TEMP_FILE がありません。: $!");
	}
	my $size = -s $ERROR_TEMP_FILE;
	if(!open(FILE, "$ERROR_TEMP_FILE")) {
		&ErrorPrint2("テンプレートファイル <tt>$ERROR_TEMP_FILE</tt> をオープンできませんでした。 : $!");
		exit;
	}
	binmode(FILE);
	my $html;
	sysread(FILE, $html, $size);
	close(FILE);
	$html =~ s/\$ERROR\$/$msg/gi;
	select(STDOUT);
	print $q->header(-type=>'text/html; charset=Shift_JIS');
	print "$html\n";
	exit;
}

sub ErrorPrint2 {
	my($msg) = @_;
	select(STDOUT);
	print $q->header(-type=>'text/html; charset=Shift_JIS');
	print "$msg\n";
	exit;
}


# 現在のタイムスタンプを返す
sub GetDate {
	my($sec, $min, $hour, $mday, $mon, $year, $wday) = localtime(time);
	$year += 1900;
	$mon += 1;
	$mon = "0$mon" if($mon < 10);
	$mday = "0$mday" if($mday < 10);
	$hour = "0$hour" if($hour < 10);
	$min = "0$min" if($min < 10);
	$sec = "0$sec" if($sec < 10);
	my(@WeekDayList) = ('日', '月', '火', '水', '木', '金', '土');
	my($DispStamp) = "$year年$mon月$mday日（$WeekDayList[$wday]） $hour:$min:$sec";
	my($Stamp) = $year.$mon.$mday.$hour.$min.$sec;
	return $Stamp,$DispStamp;
}

# シリアル番号を生成する
sub MakeSirial {
	my($Sirial) = "$Stamp-";
	my(@IpAddr) = split(/\./, $ENV{'REMOTE_ADDR'});
	my($key, $part);
	for $key (@IpAddr) {
		if($key < 10) {
			$part = "00$key";
		} elsif($key < 100) {
			$part = "0$key";
		} else {
			$part = $key;
		}
		$Sirial .= $part;
	}
	return $Sirial;
}


sub GetHostName {
	my($ip_address) = @_;
	my(@addr) = split(/\./, $ip_address);
	my($packed_addr) = pack("C4", $addr[0], $addr[1], $addr[2], $addr[3]);
	my($name, $aliases, $addrtype, $length, @addrs);
	($name, $aliases, $addrtype, $length, @addrs) = gethostbyaddr($packed_addr, 2);
	return $name;
}

# 改行コードを \n に統一
sub UnifyReturnCode {
	my($String) = @_;
	$String =~ s/\x0D\x0A/\n/g;
	$String =~ s/\x0D/\n/g;
	$String =~ s/\x0A/\n/g;
	return $String;
}

# MIME Multipart用の境界を生成
sub GenerateBoundary {
	my($BodyString) = @_;
	my(@Lines) = split(/\n/, $BodyString);
	my($Boundary, $Buff, $i, $Line);
	my($Flag) = 1;
	while($Flag) {
		$Boundary = '';
		for ($i=0;$i<20;$i++) {
			if($i % 5 == 0 && $i != 0) {$Boundary .= '_';}
			srand;
			$Boundary .= substr($Base64Table, int(rand(60)), 1);
		}
		$Boundary .= '_'.time;
		$Flag = 0;
		for $Line (@Lines) {
			if($Line =~ /$Boundary/) {$Flag = 1; last;}
		}
	}
	return $Boundary;
}

# 添付ファイルをBase64エンコード
sub Base64Encode {
	my($File) = @_;
	my($MaxCharNum) = 76;	#１行の文字数の定義

	my($FileSize) = -s "$File";
	my($Data);
	open(FILE, "$File");
	read FILE, $Data, $FileSize;
	close(FILE);

	my($EncodedData, $chunk, $ByteChunk, $PackedByteChunk, $DecimalNum);
	my($BitStream) = unpack("B*", $Data);

	my $i = 0;
	while($chunk = substr($BitStream, $i*6, 6)) {
		unless(length($chunk) == 6) {
			$chunk = pack("B6", $chunk);
			$chunk = unpack("B6", $chunk);
		}
		$ByteChunk = sprintf("%08d", $chunk);
		$PackedByteChunk = pack("B8", $ByteChunk);
		$DecimalNum = unpack("C", $PackedByteChunk);
		$EncodedData .= substr($Base64Table, $DecimalNum, 1);
		$i++;
	}

	my($PadNum) = 3 - ($FileSize % 3);
	if($PadNum == 1) {
		$EncodedData .= '=';
	} elsif($PadNum == 2) {
		$EncodedData .= '==';
	}

	my($ReformedData, $LineString);
	$i = 0;
	while($LineString = substr($EncodedData, $i*$MaxCharNum, $MaxCharNum)) {
		$ReformedData .= $LineString."\n";
		$i ++;
	}
	$ReformedData =~ s/\n$//;
	return $ReformedData;
}

#メールサブジェクトをBase64 Bエンコード
sub EncodeSubject {
	my($String) = @_;
	&jcode::convert(\$String, "euc");
	my($Base64Table) = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.
	                      'abcdefghijklmnopqrstuvwxyz'.
	                      '0123456789+/';
	my($chunk, $ByteChunk, $PackedByteChunk, $DecimalNum, $EncodedString);
	my($SplitedWord, @SplitedWordList, $i, $Byte, $Buff, $BitStream);
	my($KI) = 0;
	my($KO) = 0;
	my($CharNum) = 0;
	my($CharType) = 0;
	my($LineLength) = 0;
	my($CharEndFlag) = 1;
	if($String =~ /[^a-zA-Z0-9\!\"\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\/\^\_\~ ]/) {
		$i = 0;
		@SplitedWordList = ();
		while($i < length($String)) {
			$Byte = substr($String, $i, 1);
			if($Byte =~ /[\x8E\xA1-\xFE]/) {
				unless($CharType eq 'K') {$KI ++;}
				$CharType = 'K';
				if($CharEndFlag) {
					$CharEndFlag = 0;
				} else {
					$CharEndFlag = 1;
				}
			} else {
				if($CharType eq 'K') {$KO ++;}
				$CharType = 'A';
				$CharEndFlag = 1;
			}
			$Buff .= $Byte;
			$CharNum += 1;
			$LineLength = 27 + ($CharNum*4/3) + (($KI+$KO)*4) + 2;
			if($CharType eq 'K') {$LineLength += 4;}
			if($CharEndFlag && $LineLength>=70) {
				&jcode::convert(\$Buff, "jis");
				push(@SplitedWordList, $Buff);
				$Buff = '';
				$CharNum = 0;
				$CharType = 0;
				$KI = 0;
				$KO = 0;
			}
			$i ++;
		}
		&jcode::convert(\$Buff, "jis");
		push(@SplitedWordList, $Buff);

		for $SplitedWord (@SplitedWordList) {
			$EncodedString .= '=?ISO-2022-JP?B?';
			$BitStream = unpack("B*", $SplitedWord);
			$i = 0;
			while($chunk = substr($BitStream, $i*6, 6)) {
				unless(length($chunk) == 6) {
					$chunk = pack("B6", $chunk);
					$chunk = unpack("B6", $chunk);
				}
				$ByteChunk = sprintf("%08d", $chunk);
				$PackedByteChunk = pack("B8", $ByteChunk);
				$DecimalNum = unpack("C", $PackedByteChunk);
				$EncodedString .= substr($Base64Table, $DecimalNum, 1);
				$i++;
			}
			if(length($SplitedWord) % 3 == 1) {
				$EncodedString .= '==';
			} elsif(length($SplitedWord) % 3 == 2) {
				$EncodedString .= '=';
			}
			$EncodedString .= '?='."\n ";
		}
		$EncodedString =~ s/\n $//;
	} else {
		$EncodedString = $String;
	}
	return $EncodedString;
}

sub EucZenKanma {
	my($Value) = @_;
	my($EucZenKanma) = '，';
	&jcode::convert(\$EucZenKanma, "euc");
	$Value =~ s/,/$EucZenKanma/g;
	return $Value;
}

sub EucZenSpace {
	my($Value) = @_;
	my $EucZenSpace = '　';
	&jcode::convert(\$EucZenSpace, "euc");
	$Value =~ s/ /$EucZenSpace/g;
	return $Value;
}

sub MailAddressCheck {
	my($email) = @_;
	unless($email=~/^\w[\w\-\.]*\@\w[\w\-\.]+[a-zA-Z]{2}$/) {
		&ErrorPrint("メールアドレスが正しくありません。");
	}
	if($email =~ /,/) {
		&ErrorPrint("メールアドレスが正しくありません。");
	}
}

sub Lock {
	local(*FILE) = @_;
	my($retry) = 3;
	my($lockresult);
	while($retry-- > 0) {
		$lockresult = eval("flock(FILE, 2)");
		if($@) {
			$retry = -1;
			last;
		}
		if(!$lockresult) {
			sleep(2);
		} else {
			last;
		}
	}
	if($retry > 0) {
		return 1;
	} else {
		return 0;
	}
}

sub GetRemoteHost {
	my($remote_host);
	if($ENV{'REMOTE_HOST'} =~ /[^0-9\.]/) {
		$remote_host = $ENV{'REMOTE_HOST'};
	} else {
		my(@addr) = split(/\./, $ENV{'REMOTE_ADDR'});
		my($packed_addr) = pack("C4", $addr[0], $addr[1], $addr[2], $addr[3]);
		my($aliases, $addrtype, $length, @addrs);
		($remote_host, $aliases, $addrtype, $length, @addrs) = gethostbyaddr($packed_addr, 2);
		unless($remote_host) {
			$remote_host = $ENV{'REMOTE_ADDR'};
		}
	}
	return $remote_host;
}

sub ConfCheck {
	unless($MAILTO) {
		&ErrorPrint("メール送信先アドレスの設定をして下さい。: \$MAILTO");
	}
	unless($REDIRECT_URL) {
		&ErrorPrint("リダイレクト先URLの設定をして下さい。: \$REDIRECT_URL");
	}
	if($REPLY_FLAG) {
		unless($FROM_ADDR_FOR_REPLY) {
			&ErrorPrint("自動返信メール設定がONの場合には、必ず「自動返信メール用送信元メールアドレス」を設定してください。 : \$FROM_ADDR_FOR_REPLY");
		}
		unless($SUBJECT_FOR_REPLY) {
			&ErrorPrint("自動返信メール設定がONの場合には、必ず「自動返信メール用サブジェクト」を設定してください。 : \$SUBJECT_FOR_REPLY");
		}
	}
}


sub ExternalRequestCheck {
	my $url;
	if(scalar @ALLOW_FROM_URLS) {
		my $flag = 0;
		for $url (@ALLOW_FROM_URLS) {
			if($ENV{'HTTP_REFERER'} =~ /^$url/) {
				$flag = 1;
			}
		}
		unless($flag) {
			&ErrorPrint("不正なサーバからのリクエストです。");
		}
	}


}

sub GetCgiUrl {
	my @url_parts = split(/\//, $ENV{'SCRIPT_NAME'});
	my $script_filename = pop @url_parts;
	return $script_filename;
}

sub RejectHostAccess {
	my($HostName) = @_;
	my($Reject);
	my $RejectFlag = 0;
	if(scalar @REJECT_HOSTS) {
		for $Reject (@REJECT_HOSTS) {
			if($Reject =~ /[^0-9\.]/) {	# ホスト名指定の場合
				if($HostName =~ /$Reject$/) {
					$RejectFlag = 1;
					last;
				}
			} else {	# IPアドレス指定の場合
				if($ENV{'REMOTE_ADDR'} =~ /^$Reject/) {
					$RejectFlag = 1;
					last;
				}
			}
		}
		if($RejectFlag) {
			&ErrorPrint("$REJECT_ERR_MSG");
			exit;
		}
	}
}


sub WordWrap {
	my($Str) = @_;
	&jcode::convert(\$Str, 'euc');

	my $WrapedStr = '';
	my $LineLength = 0;
	my($word, $WordLength);
	my(@SplitedWords, $buff, $byte, $i, @Words, @WordParts, $part);
	my @Lines = split(/\n/, $Str);
	my $line;
	my $KanjiFlag = 0;
	my $KanjiFirstByteFlag = 1;
	for $line (@Lines) {
		@Words = split(/ /, $line);
		for $word (@Words) {
			@WordParts = ();
			$i = 0;
			$KanjiFirstByteFlag = 1;
			$part = '';
			while($i <= length($word) - 1) {
				$byte = substr($word, $i, 1);
				if($byte =~ /[\x8E\xA1-\xFE]/) {	#日本語
					unless($KanjiFlag) {
						push(@WordParts, $part);
						$part = '';
						$KanjiFirstByteFlag = 1;
						$KanjiFlag = 1;
					}
					$part .= "$byte";
					if($KanjiFirstByteFlag) {
						$KanjiFirstByteFlag = 0;
					} else {
						$KanjiFirstByteFlag = 1;
						push(@WordParts, $part);
						$part = '';
					}
				} else {				#英語
					if($KanjiFlag) {
						$part = '';
					}
					$part .= "$byte";
					if($byte =~ /^(\-|\/)$/) {
						push(@WordParts, $part);
						$part = '';
					}
					$KanjiFlag = 0;
					$KanjiFirstByteFlag = 1;
				}
				$i ++;
			}
			push(@WordParts, $part);
			for $part (@WordParts) {
				if($LineLength + length($part) > $WRAP) {
					$WrapedStr .= "\n";
					$LineLength = 0;
				}
				$WrapedStr .= "$part";
				$LineLength += length($part);
			}
			$WrapedStr .= " ";
			$LineLength += 1;
		}
		$WrapedStr .= "\n";
		$LineLength = 0;
	}
	return $WrapedStr;
}


